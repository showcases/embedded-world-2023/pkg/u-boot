From: Christopher Obbard <chris.obbard@collabora.com>
Date: Thu, 2 Mar 2023 12:00:00 +0000
Subject: Autodetect camera at runtime and load correct devicetree

The ROCK4 can have multiple cameras connected to the ribbon cable; attempt
to probe each camera at runtime and load the devicetree based on the
detected camera.

Signed-off-by: Christopher Obbard <chris.obbard@collabora.com>
---
 arch/arm/dts/rk3399-rock-pi-4b-u-boot.dtsi | 12 ++++++
 board/rockchip/evb_rk3399/evb-rk3399.c     | 65 +++++++++++++++++++++++++++++-
 2 files changed, 76 insertions(+), 1 deletion(-)

diff --git a/arch/arm/dts/rk3399-rock-pi-4b-u-boot.dtsi b/arch/arm/dts/rk3399-rock-pi-4b-u-boot.dtsi
index 85ee577..9756bdb 100644
--- a/arch/arm/dts/rk3399-rock-pi-4b-u-boot.dtsi
+++ b/arch/arm/dts/rk3399-rock-pi-4b-u-boot.dtsi
@@ -4,3 +4,15 @@
  */
 
 #include "rk3399-rock-pi-4-u-boot.dtsi"
+
+/* HACK: Enable I2C communication to cameras */
+/ {
+	vcc_cam_sensor: vcc-cam-sensor {
+		compatible = "regulator-fixed";
+		regulator-name = "vcc_cam_sensor";
+
+		gpio = <&gpio1 RK_PB5 GPIO_ACTIVE_HIGH>;
+		enable-active-high;
+		vin-supply = <&vcc_cam>;
+	};
+};
diff --git a/board/rockchip/evb_rk3399/evb-rk3399.c b/board/rockchip/evb_rk3399/evb-rk3399.c
index f56b379..b20efe2 100644
--- a/board/rockchip/evb_rk3399/evb-rk3399.c
+++ b/board/rockchip/evb_rk3399/evb-rk3399.c
@@ -11,8 +11,15 @@
 #include <asm/arch-rockchip/periph.h>
 #include <linux/kernel.h>
 #include <power/regulator.h>
+#include <linux/delay.h>
+#include <env.h>
+#include <i2c.h>
+#include <dm/uclass.h>
 
-#define ROCKPI4_UPDATABLE_IMAGES	2
+#define ROCKPI4_UPDATABLE_IMAGES		2
+#define CAMERA_SENSOR_I2C_BUS			4
+#define CAMERA_SENSOR_I2C_ADDR_IMX219	0x10
+#define CAMERA_SENSOR_I2C_ADDR_OV5647	0x36
 
 #if CONFIG_IS_ENABLED(EFI_HAVE_CAPSULE_SUPPORT)
 static struct efi_fw_image fw_images[ROCKPI4_UPDATABLE_IMAGES] = {0};
@@ -44,6 +51,62 @@ out:
 	return 0;
 }
 
+int rk_board_late_init(void)
+{
+	struct udevice *regulator, *bus, *dummy;
+	int ret;
+
+	/* HACK: Autodetect connected camera and load fdt */
+	printf("Autodetecting connected camera...\n");
+	ret = regulator_get_by_platname("vcc_cam_sensor", &regulator);
+	if (ret) {
+		debug("%s vcc_cam_sensor init fail! ret %d\n", __func__, ret);
+		goto out;
+	}
+
+	/* Probe i2c device */
+	ret = uclass_get_device_by_seq(UCLASS_I2C, CAMERA_SENSOR_I2C_BUS, &bus);
+	if (ret) {
+		debug("%s i2c init fail! ret %d\n", __func__, ret);
+		goto out;
+	}
+
+	/* Turn the Camera power GPIO on */
+	ret = regulator_set_enable(regulator, true);
+	if (ret) {
+		debug("%s vcc_cam_sensor set power fail! ret %d\n", __func__, ret);
+		goto out;
+	}
+
+	/* Wait for the sensor to power up */
+	mdelay(100);
+
+	/* Detect sensor by probing the address on the bus */
+	ret = dm_i2c_probe(bus, CAMERA_SENSOR_I2C_ADDR_IMX219, 0, &dummy);
+	if (ret == 0) {
+		printf("Found IMX219!\n");
+		env_set("fdtfile", "rockchip/rk3399-rock-pi-4b-imx219.dtb");
+		goto out_camera;
+	} else {
+		printf("Probing IMX219 failed; ret %d\n", ret);
+	}
+
+	ret = dm_i2c_probe(bus, CAMERA_SENSOR_I2C_ADDR_OV5647, 0, &dummy);
+	if (ret == 0) {
+		printf("Found OV5647!\n");
+		env_set("fdtfile", "rockchip/rk3399-rock-pi-4b-ov5647.dtb");
+	} else {
+		printf("Probing OV5647 failed; ret %d\n", ret);
+	}
+
+out_camera:
+	/* Turn the Camera power GPIO off */
+	regulator_set_enable(regulator, false);
+
+out:
+	return 0;
+}
+
 #if defined(CONFIG_EFI_HAVE_CAPSULE_SUPPORT) && defined(CONFIG_EFI_PARTITION)
 static bool board_is_rockpi_4b(void)
 {
